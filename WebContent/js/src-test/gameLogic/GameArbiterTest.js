GameArbiterTestCase = TestCase("GameArbiterTestCase");

var GAME_ARBITER = new GameArbiter();

GameArbiterTestCase.prototype.testStartNewGame = function() {
	GAME_ARBITER.startNewGame(PlayerOwner.CPU);
	assertEquals(new Board(3, 3, 3), GAME_ARBITER.currentBoardCopy);
};

GameArbiterTestCase.prototype.testMoveCpuFirst = function() {
	GAME_ARBITER.startNewGame(PlayerOwner.CPU);
	var playerStatus = null;
	
	playerStatus = GAME_ARBITER.getCpuMove();
	assertEquals(PlayerStatus.NONE, playerStatus);
	assertEquals("X__\n" +
				 "___\n" +
				 "___\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("X__\n" +
				 "___\n" +
				 "___\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(0, GAME_ARBITER.currentGameStateNode.score);
	
	playerStatus = GAME_ARBITER.move(1,0);
	assertEquals(PlayerStatus.NONE, playerStatus);
	assertEquals("XO_\n" +
				 "___\n" +
				 "___\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("XO_\n" +
				 "___\n" +
				 "___\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(1, GAME_ARBITER.currentGameStateNode.score);
	
	playerStatus = GAME_ARBITER.getCpuMove();
	assertEquals(PlayerStatus.NONE, playerStatus);
	assertEquals("XO_\n" +
				 "X__\n" +
				 "___\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("XO_\n" +
				 "X__\n" +
				 "___\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(1, GAME_ARBITER.currentGameStateNode.score);
	
	playerStatus = GAME_ARBITER.move(1,1);
	assertEquals(PlayerStatus.NONE, playerStatus);
	assertEquals("XO_\n" +
				 "XO_\n" +
				 "___\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("XO_\n" +
				 "XO_\n" +
				 "___\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(1, GAME_ARBITER.currentGameStateNode.score);
	
	playerStatus = GAME_ARBITER.getCpuMove();
	assertEquals(PlayerStatus.PLAYER_ONE_WIN, playerStatus);
	assertEquals("XO_\n" +
				 "XO_\n" +
				 "X__\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("XO_\n" +
				 "XO_\n" +
				 "X__\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(1, GAME_ARBITER.currentGameStateNode.score);
};

GameArbiterTestCase.prototype.testMoveHumanFirst = function() {
	GAME_ARBITER.startNewGame(PlayerOwner.HUMAN);
	var playerStatus = null;
	
	playerStatus = GAME_ARBITER.move(2,0);
	assertEquals(PlayerStatus.NONE, playerStatus);
	assertEquals("__X\n" +
				 "___\n" +
				 "___\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("__X\n" +
				 "___\n" +
				 "___\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(0, GAME_ARBITER.currentGameStateNode.score);
	
	playerStatus = GAME_ARBITER.getCpuMove();
	assertEquals(PlayerStatus.NONE, playerStatus);
	assertEquals("__X\n" +
				 "_O_\n" +
				 "___\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("__X\n" +
				 "_O_\n" +
				 "___\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(0, GAME_ARBITER.currentGameStateNode.score);
	
	playerStatus = GAME_ARBITER.move(0,2);
	assertEquals(PlayerStatus.NONE, playerStatus);
	assertEquals("__X\n" +
				 "_O_\n" +
				 "X__\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("__X\n" +
				 "_O_\n" +
				 "X__\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(0, GAME_ARBITER.currentGameStateNode.score);
	
	playerStatus = GAME_ARBITER.getCpuMove();
	assertEquals(PlayerStatus.NONE, playerStatus);
	assertEquals("_OX\n" +
				 "_O_\n" +
				 "X__\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("_OX\n" +
				 "_O_\n" +
				 "X__\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(0, GAME_ARBITER.currentGameStateNode.score);
	
	playerStatus = GAME_ARBITER.move(1,2);
	assertEquals(PlayerStatus.NONE, playerStatus);
	assertEquals("_OX\n" +
				 "_O_\n" +
				 "XX_\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("_OX\n" +
				 "_O_\n" +
				 "XX_\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(0, GAME_ARBITER.currentGameStateNode.score);
	
	playerStatus = GAME_ARBITER.getCpuMove();
	assertEquals(PlayerStatus.NONE, playerStatus);
	assertEquals("_OX\n" +
				 "_O_\n" +
				 "XXO\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("_OX\n" +
				 "_O_\n" +
				 "XXO\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(0, GAME_ARBITER.currentGameStateNode.score);
	
	playerStatus = GAME_ARBITER.move(0,0);
	assertEquals(PlayerStatus.NONE, playerStatus);
	assertEquals("XOX\n" +
				 "_O_\n" +
				 "XXO\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("XOX\n" +
				 "_O_\n" +
				 "XXO\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(0, GAME_ARBITER.currentGameStateNode.score);
	
	playerStatus = GAME_ARBITER.getCpuMove();
	assertEquals(PlayerStatus.NONE, playerStatus);
	assertEquals("XOX\n" +
				 "OO_\n" +
				 "XXO\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("XOX\n" +
				 "OO_\n" +
				 "XXO\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(0, GAME_ARBITER.currentGameStateNode.score);
	
	playerStatus = GAME_ARBITER.move(2,1);
	assertEquals(PlayerStatus.DRAW, playerStatus);
	assertEquals("XOX\n" +
				 "OOX\n" +
				 "XXO\n",GAME_ARBITER.currentGameStateNode.gameState.board.toString("\n"));
	assertEquals("XOX\n" +
				 "OOX\n" +
				 "XXO\n",GAME_ARBITER.currentBoardCopy.toString("\n"));
	assertEquals(0, GAME_ARBITER.currentGameStateNode.score);
	
};

GameArbiterTestCase.prototype.testStartNewGame4x3 = function() {
	CELLS_X = 4;
	GAME_ARBITER.startNewGame(PlayerOwner.HUMAN);
	assertEquals(new Board(4, 3, 3), GAME_ARBITER.currentBoardCopy);
	var playerStatus = GAME_ARBITER.move(1, 1);
	GAME_ARBITER.getCpuMove();
	
	
	CELLS_X = 3;
};