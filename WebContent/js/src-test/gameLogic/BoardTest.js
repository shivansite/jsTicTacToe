BoardTestCase = TestCase("BoardTestCase");

BoardTestCase.prototype.testToString = function() {
	var board1 = new Board(3, 3, 3);
	
	var player1 = new PlayerFactory().createPlayerOne();
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 1, 0);		
	board1.move(player2, 1, 1);
	board1.move(player2, 2, 1);
	
	assertEquals("XX_<br>_OO<br>___<br>", board1.toString("<br>"));
};