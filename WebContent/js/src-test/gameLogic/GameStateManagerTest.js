GameTestManagerTestCase = TestCase("GameTestManagerTestCase");

var GAME_TEST_MANAGER = new GameStateManager();

GameTestManagerTestCase.prototype.testCheckWinStatus1 = function() {
	var board1 = new Board(3, 3, 3);
	
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.NONE, winStatus);
	
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 1, 0);		
	board1.move(player2, 1, 1);
	board1.move(player2, 2, 1);
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.NONE, winStatus);
};

GameTestManagerTestCase.prototype.testCheckWinStatus2 = function() {
	var board1 = new Board(3, 3, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 1, 0);		
	board1.move(player1, 2, 0);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_ONE_WIN, winStatus);
	
	board1 = new Board(3, 3, 3);		
	var player1 = new PlayerFactory().createPlayerOne();
	var player2 = player1.opponent;		
	board1.move(player2, 0, 1);
	board1.move(player2, 1, 1);		
	board1.move(player2, 2, 1);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_TWO_WIN, winStatus);
	
	board1 = new Board(3, 3, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player2, 0, 1);
	board1.move(player2, 1, 1);		
	board1.move(player1, 0, 2);
	board1.move(player1, 1, 2);		
	board1.move(player1, 2, 2);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_ONE_WIN, winStatus);
	
};

GameTestManagerTestCase.prototype.testCheckWinStatus3 = function() {
	var board1 = new Board(3, 3, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 0, 1);		
	board1.move(player1, 0, 2);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_ONE_WIN, winStatus);
	
	board1 = new Board(3, 3, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player2, 1, 0);
	board1.move(player2, 1, 1);		
	board1.move(player2, 1, 2);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_TWO_WIN, winStatus);
	
	board1 = new Board(3, 3, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player2, 1, 0);
	board1.move(player2, 1, 1);		
	board1.move(player1, 2, 0);
	board1.move(player1, 2, 1);		
	board1.move(player1, 2, 2);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_ONE_WIN, winStatus);
	
};

GameTestManagerTestCase.prototype.testCheckWinStatus4 = function() {
	var board1 = new Board(3, 3, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 1, 1);		
	board1.move(player1, 2, 2);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_ONE_WIN, winStatus);
	
	board1 = new Board(3, 3, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player2, 2, 0);
	board1.move(player2, 1, 1);		
	board1.move(player2, 0, 2);	
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_TWO_WIN, winStatus);
};

GameTestManagerTestCase.prototype.testCheckWinStatus5 = function() {
	var board1 = new Board(3, 3, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 1, 0);		
	board1.move(player2, 2, 0);		
	board1.move(player2, 0, 1);
	board1.move(player1, 1, 1);		
	board1.move(player1, 2, 1);	
	board1.move(player1, 0, 2);
	board1.move(player2, 1, 2);		
	board1.move(player2, 2, 2);	
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.DRAW, winStatus);
};

GameTestManagerTestCase.prototype.testCheckWinStatus_4x4_1 = function() {
	var board1 = new Board(4, 4, 3);
	
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.NONE, winStatus);
	
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 1, 0);		
	board1.move(player2, 1, 1);
	board1.move(player2, 2, 1);
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.NONE, winStatus);
};

GameTestManagerTestCase.prototype.testCheckWinStatus_4x4_2 = function() {
	var board1 = new Board(4, 4, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 1, 0);		
	board1.move(player1, 2, 0);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_ONE_WIN, winStatus);
	
	board1 = new Board(4, 4, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player2, 0, 1);
	board1.move(player2, 1, 1);		
	board1.move(player2, 2, 1);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_TWO_WIN, winStatus);
	
	board1 = new Board(4, 4, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player2, 0, 1);
	board1.move(player2, 1, 1);		
	board1.move(player1, 0, 2);
	board1.move(player1, 1, 2);		
	board1.move(player1, 2, 2);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_ONE_WIN, winStatus);
	
};

GameTestManagerTestCase.prototype.testCheckWinStatus_4x4_3 = function() {
	var board1 = new Board(4, 4, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 0, 1);		
	board1.move(player1, 0, 2);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_ONE_WIN, winStatus);
	
	board1 = new Board(4, 4, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player2, 1, 0);
	board1.move(player2, 1, 1);		
	board1.move(player2, 1, 2);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_TWO_WIN, winStatus);
	
	board1 = new Board(4, 4, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player2, 1, 0);
	board1.move(player2, 1, 1);		
	board1.move(player1, 2, 0);
	board1.move(player1, 2, 1);		
	board1.move(player1, 2, 2);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_ONE_WIN, winStatus);
	
};

GameTestManagerTestCase.prototype.testCheckWinStatus_4x4_4 = function() {
	var board1 = new Board(4, 4, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 1, 1);		
	board1.move(player1, 2, 2);		
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_ONE_WIN, winStatus);
	
	board1 = new Board(4, 4, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player2, 2, 0);
	board1.move(player2, 1, 1);		
	board1.move(player2, 0, 2);	
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.PLAYER_TWO_WIN, winStatus);
};

GameTestManagerTestCase.prototype.testCheckWinStatus_4x4_5 = function() {
	var board1 = new Board(4, 4, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 1, 0);		
	board1.move(player2, 2, 0);		
	board1.move(player2, 0, 1);
	board1.move(player1, 1, 1);		
	board1.move(player1, 2, 1);	
	board1.move(player1, 0, 2);
	board1.move(player2, 1, 2);		
	board1.move(player2, 2, 2);	
	var winStatus = GAME_TEST_MANAGER.checkWinStatus(board1);
	assertEquals(PlayerStatus.NONE, winStatus);
};

GameTestManagerTestCase.prototype.testGetNextPossiblBoardStates1 = function() {
	var board1 = new Board(3, 3, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 1, 0);		
	board1.move(player2, 2, 0);		
	board1.move(player2, 0, 1);
	board1.move(player1, 1, 1);		
	board1.move(player1, 2, 1);	
	board1.move(player1, 0, 2);
	board1.move(player2, 1, 2);	
	
	var nextBoardStates = GAME_TEST_MANAGER.getNextPossiblBoardStates(player2, board1);
	assertEquals(1, nextBoardStates.length);
	
	assertEquals(player2.playerTokenType, nextBoardStates[0].getTokenAt(2, 2));
};

GameTestManagerTestCase.prototype.testGetNextPossiblBoardStates2 = function() {
	var board1 = new Board(3, 3, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 1, 0);		
	board1.move(player2, 2, 0);		
	board1.move(player2, 0, 1);
	board1.move(player1, 1, 1);		
	board1.move(player1, 2, 1);	
	board1.move(player1, 0, 2);
	
	var nextBoardStates = GAME_TEST_MANAGER.getNextPossiblBoardStates(player2, board1);
	assertEquals(2, nextBoardStates.length);		
	
};

GameTestManagerTestCase.prototype.testGetNextPossiblBoardStates3 = function() {
	var board1 = new Board(3, 3, 3);		
	var player1 = new PlayerFactory().createPlayerOne(PlayerOwner.CPU);
	var player2 = player1.opponent;		
	board1.move(player1, 0, 0);
	board1.move(player1, 1, 0);		
	board1.move(player1, 2, 0);		
	board1.move(player2, 0, 1);
	board1.move(player2, 1, 1);
	
	var nextBoardStates = GAME_TEST_MANAGER.getNextPossiblBoardStates(player2, board1);
	assertEquals(0, nextBoardStates.length);		
	
};