function Player(playerOwner) {	
	
	this.playerOwner = playerOwner;
	this.playerTokenType = TokenType.X;		
	this.opponent = null;	
}

function PlayerFactory() {
	
	this.createPlayerOne = function(playerOneOwner) {
		var player1 = new Player(playerOneOwner);
		var player2 = new Player(playerOneOwner == PlayerOwner.CPU ? PlayerOwner.HUMAN : PlayerOwner.CPU);
		
		player2.playerTokenType = TokenType.O;
		
		player1.opponent = player2;
		player2.opponent = player1;
		
		return player1;
	};
}