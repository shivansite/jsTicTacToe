/**
 * @constructor
 * @param {Object} firstState
 * @returns {FiniteStateMachine}
 */
FiniteStateMachine = function(firstState) {
	this.currentState = firstState;
	this.fsmContext = {};	
};

/**
 * @public
 */
FiniteStateMachine.prototype.onProcessInput = function(inputEvent) {
	this.currentState.onProcessInput(inputEvent, this.fsmContext);
};

/**
 * @public 
 */
FiniteStateMachine.prototype.onUpdateState = function() {
	this.currentState = this.currentState.onUpdateState(this.fsmContext);
};