function GameArea(img, imgInactive, imgLoading, cells, cellsX, cellsY) {
	
	$.extend(this, new GameEntity(0, 0, CELLS_X * CELL_SIZE, CELLS_Y * CELL_SIZE));
	
	this.img = img;
	this.imgInactive = imgInactive;
	this.imgLoading = imgLoading;
	
	this.isInactive = true;
	this.isLoading = false;
	/**
	 * @public
	 */
	this.processInput = function(inputEvent) {

	};
	
	/**
	 * @public
	 */
	this.updateState = function() {

	};
	
	this.setInactive = function(inactive) {
		this.isInactive = inactive;
		this.isLoading = false;
	};
	
	this.setLoading = function(loading) {
		this.isInactive = false;
		this.isLoading = loading;
	};
	
	/**
	 * @public
	 * @param{Context} canvas
	 */
	this.updateGraphics = function(context) {
		context.drawImage(this.img, 0, 0, this.width, this.height);
		if(this.isInactive) {
			context.drawImage(this.imgInactive, 0, 0, this.width, this.height);
		} else if(this.isLoading) {
			context.drawImage(this.imgLoading, 0, 0, this.width, this.height);
		}
	};
};