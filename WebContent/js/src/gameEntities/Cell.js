function Cell(cellX, cellY, imgHighlight, imgTokenX, imgTokenO) {

	$.extend(this, new GameEntity(cellX * CELL_SIZE, cellY * CELL_SIZE, CELL_SIZE, CELL_SIZE));
	
	this.cellX = cellX;
	this.cellY = cellY;
	
	this.imgHighlight = imgHighlight;
	this.imgTokenX = imgTokenX;
	this.imgTokenO = imgTokenO;
	
	this.isHighlighted = false;
	this.tokenType = TokenType.NONE;
	this.imgTokenCurrent = null;
	
	this.isInactive = true;
	
	/**
	 * @public
	 */
	this.processInput = function(inputEvent) {
		this.isHighlighted = false;
		if(this.isInactive) {
			return;
		}
		if(inputEvent.mouseEventType == MouseEventType.MOUSE_MOVE) {
			if(this.isMouseIn(inputEvent.x, inputEvent.y)) {
				this.isHighlighted = true;
			}
		}		
	};
	
	/**
	 * @public
	 */
	this.updateState = function() {
		if(TokenType.X == this.tokenType) {
			this.imgTokenCurrent = this.imgTokenX;
			this.isHighlighted = false;
		} else if(TokenType.O == this.tokenType) {
			this.imgTokenCurrent = this.imgTokenO;
			this.isHighlighted = false;
		} else if(this.tokenType == TokenType.NONE) {
			this.imgTokenCurrent = null;
		}
	};
	
	this.setToken = function(tokenType) {
		this.tokenType = tokenType;
	};
	
	this.setInactive = function(inactive) {
		this.isInactive = inactive;
	};
	
	/**
	 * @public
	 * @param{Context} canvas
	 */
	this.updateGraphics = function(context) {
		if(this.isInactive) {
			return;
		}
		if(this.imgTokenCurrent != null) {
			context.drawImage(this.imgTokenCurrent, this.startX, this.startY, this.width, this.height);
		}
		if(this.isHighlighted) {
			context.drawImage(this.imgHighlight, this.startX, this.startY, this.width, this.height);
		}
	};
	
	this.isMouseIn = function(mouseX, mouseY) {
		if(mouseX >= this.startX && mouseX < this.startX + this.width &&
				mouseY >= this.startY && mouseY < this.startY + this.height) {
			return true;
		}
	};
};