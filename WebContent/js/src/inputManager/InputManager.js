/**
 * @constructor
 * @param(Queue) inputQueue
 */
function CanvasMouseListener(inputQueue) {
	
	this.inputQueue = inputQueue;
	
	var self = this;
	$("#canvas").bind( "mouseup", function(e){ self.onMouseUp(e); } );
	$("#canvas").bind( "mousedown", function(e){ self.onMouseDown(e); } );
	$("#canvas").bind( "mousemove", function(e){ self.onMouseMove(e); } );
	
	this.onMouseUp = function(e) {
		if(e.which==1) {
			this.createMouseEvent(e, MouseEventType.MOUSE_LEFT_UP);
		} else if(e.which==3) {
			this.createMouseEvent(e, MouseEventType.MOUSE_RIGHT_UP);
		}
		
	};
	
	this.onMouseDown = function(e) {
		if(e.which==1) {
			this.createMouseEvent(e, MouseEventType.MOUSE_LEFT_DOWN);
		} else if(e.which==3) {
			this.createMouseEvent(e, MouseEventType.MOUSE_RIGHT_DOWN);
		}
	};
	
	this.onMouseMove = function(e) {
		this.createMouseEvent(e, MouseEventType.MOUSE_MOVE);
	};
	
	this.createMouseEvent = function(e, mouseEventType) {
		var xpos, ypos;
		if(e.offsetX==undefined) {//firefox
		    xpos = e.pageX-$('#canvas').offset().left;
		    ypos = e.pageY-$('#canvas').offset().top;
		} else {//others
		    xpos = e.offsetX;
		    ypos = e.offsetY;
		}
		if(xpos >= 0 && ypos >= 0 && xpos < $('#canvas').width() && ypos < $('#canvas').height()) {
			var event = new MouseEvent(xpos, ypos, mouseEventType);
			this.inputQueue.push(event);
		}		
	};
};

/**
 * @Constructor
 */
function MouseEvent(x, y, mouseEventType) {
	this.x = x;
	this.y = y;
	this.mouseEventType = mouseEventType;
};

/**
 * @constructor
 * @param capacity
 * @returns {Queue}
 */
function Queue(capacity) {
	
	this.capacity = capacity;
	this.container = [];
	this.size = 0;
	
	/**
	 * @public
	 * @param element
	 */
	this.push = function(element) {
		if(this.size<this.capacity) {
			this.container.push(element);
			this.size++;
			return true;
		}
		return false;
	};
	
	/**
	 * @public
	 */
	this.pop = function() {
		if(this.size==0) {
			return null;
		}
		var element = this.container.shift();
		this.size--;
		return element;
	};
	
	/**
	 * @public
	 * @returns {Boolean}
	 */
	this.isEmpty = function() {
		return this.size==0;
	};
	
	/**
	 * @public
	 * @returns {Boolean}
	 */
	this.isFull = function() {
		return this.size==this.capacity;
	};
};

function ControlsInputManager(inputQueue) {
	
	this.inputQueue = inputQueue;
	
	this.firstPlayerOwnerSelect = $('#firstPlayerOwnerSelect');
	this.startGameButton = $('#startGameButton');
	
	var self = this;
	$('#startGameButton').bind( "mouseup", function(e){ self.onStartGameButtonMouseUp(e); } );
	
	this.onStartGameButtonMouseUp = function(e) {
		var gameStartEvent = new GameStartEvent(PlayerOwner[this.firstPlayerOwnerSelect.val()]);
		this.inputQueue.push(gameStartEvent);
	};
};

function GameStartEvent(firstPlayerOwner) {
	this.firstPlayerOwner = firstPlayerOwner;
};

