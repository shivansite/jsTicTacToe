/**
 * @constructor
 */
function GameEntity(startX, startY, width, height) {
	
	this.startX = startX;
	this.startY = startY;
	this.width = width;
	this.height = height;
	
	/**
	 * @public
	 */
	this.processInput = function(inputEvent) {};
	
	/**
	 * @public
	 */
	this.updateState = function() {};
	
	/**
	 * @public
	 * @param{Context} canvas
	 */
	this.updateGraphics = function(context) {};
};