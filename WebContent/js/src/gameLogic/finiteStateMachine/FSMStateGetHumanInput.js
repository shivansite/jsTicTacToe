function FSMStateGetHumanInput(gameArbiter, cells) {
	this.fsmStateCheckPlayerStatus = null;
	this.currentHumanMove = null;
	this.gameArbiter = gameArbiter;
	this.cells = cells;
};

FSMStateGetHumanInput.prototype = Object.create(FSMState.prototype);
FSMStateGetHumanInput.prototype.constructor = FSMStateGetHumanInput;

FSMStateGetHumanInput.prototype.onProcessInput = function(inputEvent, fsmContext) {
	if(inputEvent.mouseEventType == MouseEventType.MOUSE_LEFT_UP) {
		var cellX = Math.floor(inputEvent.x/CELL_SIZE);
		var cellY = Math.floor(inputEvent.y/CELL_SIZE);
		for(var i = 0; i < this.cells.length; i++) {
			var cell = this.cells[i];
			if(cell.cellX == cellX && cell.cellY == cellY) {
				if(cell.tokenType != TokenType.NONE) {
					return;
				}
			}
		}
		this.currentHumanMove = {
			cellX:Math.floor(inputEvent.x/CELL_SIZE),
			cellY:Math.floor(inputEvent.y/CELL_SIZE)
		};
	}
};

FSMStateGetHumanInput.prototype.onUpdateState = function(fsmContext) {
	if(this.currentHumanMove != null) {
		this.gameArbiter.move(this.currentHumanMove.cellX, this.currentHumanMove.cellY);
		for(var i = 0; i < this.cells.length; i++) {
			var cell = this.cells[i];
			cell.setToken(this.gameArbiter.currentBoardCopy.getTokenAt(cell.cellX, cell.cellY));
		}
		this.currentHumanMove = null;
		return this.fsmStateCheckPlayerStatus;
	} else {
		return this;
	}
};