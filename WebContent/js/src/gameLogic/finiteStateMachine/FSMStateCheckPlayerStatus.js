function FSMStateCheckPlayerStatus(gameArbiter, gameOutcomeDiv) {
	this.fsmStateChoseNextPlayer = null;
	this.fsmStateGameEnded = null;
	this.gameArbiter = gameArbiter;
	this.gameOutcomeDiv = gameOutcomeDiv;
	
};

FSMStateCheckPlayerStatus.prototype = Object.create(FSMState.prototype);
FSMStateCheckPlayerStatus.prototype.constructor = FSMStateCheckPlayerStatus;

FSMStateCheckPlayerStatus.prototype.onProcessInput = function(inputEvent, fsmContext) {
	
};

FSMStateCheckPlayerStatus.prototype.onUpdateState = function(fsmContext) {
	var playerStatus = this.gameArbiter.currentGameStateNode.gameState.playerStatus;
	if(playerStatus == PlayerStatus.NONE) {
		fsmContext["nextPlayerOwner"] = fsmContext["nextPlayerOwner"] == PlayerOwner.CPU ? PlayerOwner.HUMAN : PlayerOwner.CPU;
		return this.fsmStateChoseNextPlayer;
	} else {
		$("#firstPlayerOwnerSelect").removeAttr("disabled");
		$("#startGameButton").removeAttr("disabled");
		this.gameOutcomeDiv.empty();
		$("<span>" + DisplayLabelPlayerStatus[playerStatus] + "<br></span>").appendTo(this.gameOutcomeDiv);
		return this.fsmStateGameEnded;
	}
};