function FSMStateChoseNextPlayer() {
	this.fsmStateGetHumanInput = null;
	this.fsmStateMakeCPUMove = null;
};

FSMStateChoseNextPlayer.prototype = Object.create(FSMState.prototype);
FSMStateChoseNextPlayer.prototype.constructor = FSMStateChoseNextPlayer;

FSMStateChoseNextPlayer.prototype.onProcessInput = function(inputEvent, fsmContext) {
	
};

FSMStateChoseNextPlayer.prototype.onUpdateState = function(fsmContext) {
	if(fsmContext["nextPlayerOwner"] == PlayerOwner.HUMAN) {
		return this.fsmStateGetHumanInput;
	} else if(fsmContext["nextPlayerOwner"] == PlayerOwner.CPU) {
		return this.fsmStateMakeCPUMove;
	}
};