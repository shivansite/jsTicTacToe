function FSMStateMakeCPUMove(gameArbiter, cells) {
	this.fsmStateCheckPlayerStatus = null;
	this.gameArbiter = gameArbiter;
	this.cells = cells;
};

FSMStateMakeCPUMove.prototype = Object.create(FSMState.prototype);
FSMStateMakeCPUMove.prototype.constructor = FSMStateMakeCPUMove;

FSMStateMakeCPUMove.prototype.onProcessInput = function(inputEvent, fsmContext) {};

FSMStateMakeCPUMove.prototype.onUpdateState = function(fsmContext) {
	this.gameArbiter.getCpuMove();
	for(var i = 0; i < this.cells.length; i++) {
		var cell = this.cells[i];
		cell.setToken(this.gameArbiter.currentBoardCopy.getTokenAt(cell.cellX, cell.cellY));
	}
	return this.fsmStateCheckPlayerStatus;
};