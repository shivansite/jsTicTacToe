function TicTacToeFSM(gameArbiter, gameArea, cells) {
	var gameOutcomeDiv = $("#gameOutcomeDiv");
	gameOutcomeDiv.html(" ");
	
	var fsmStateGameEnded = new FSMStateGameEnded(gameArbiter, gameArea, cells, gameOutcomeDiv);
	var fsmStateChoseNextPlayer = new FSMStateChoseNextPlayer();
	var fsmStateCheckPlayerStatus = new FSMStateCheckPlayerStatus(gameArbiter, gameOutcomeDiv);
	var fsmStateGetHumanInput = new FSMStateGetHumanInput(gameArbiter, cells);
	var fsmStateMakeCPUMove = new FSMStateMakeCPUMove(gameArbiter, cells);
	
	fsmStateGameEnded.fsmStateChoseNextPlayer = fsmStateChoseNextPlayer;
	fsmStateChoseNextPlayer.fsmStateGetHumanInput = fsmStateGetHumanInput;
	fsmStateChoseNextPlayer.fsmStateMakeCPUMove = fsmStateMakeCPUMove;
	fsmStateGetHumanInput.fsmStateCheckPlayerStatus = fsmStateCheckPlayerStatus;
	fsmStateMakeCPUMove.fsmStateCheckPlayerStatus = fsmStateCheckPlayerStatus;
	fsmStateCheckPlayerStatus.fsmStateChoseNextPlayer = fsmStateChoseNextPlayer;
	fsmStateCheckPlayerStatus.fsmStateGameEnded = fsmStateGameEnded;
	
	FiniteStateMachine.call(this, fsmStateGameEnded);
};

TicTacToeFSM.prototype = Object.create(FiniteStateMachine.prototype);
TicTacToeFSM.prototype.constructor = TicTacToeFSM;


