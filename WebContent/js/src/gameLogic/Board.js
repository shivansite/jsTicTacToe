function Board(width, height, inARow) {
	
	this.width = width;
	this.height = height;
	this.inARow = inARow;
	this.state = new Array();	
	this.tokensCount = 0;
	this.hash = null;
	
	for(var x=0; x<width; x++) {
		for(var y=0; y<height; y++) {
			this.state[this.width*y + x] = TokenType.NONE;
		}
	}
	
	this.calculateHashCode();
};
	
Board.prototype.move = function(playerToMove, x, y) {
	this.tokensCount++;
	this.state[this.width*y + x] = playerToMove.playerTokenType;
	this.calculateHashCode();
};

Board.prototype.getTokenAt = function(x, y) {
	return this.state[this.width*y + x];
};

Board.prototype.copy = function() {
	var boardCopy = new Board(this.width, this.height, this.inARow);
	for(var x = 0; x < this.width; x++) {
		for(var y = 0; y < this.height; y++) {
			boardCopy.state[this.width * y + x] = this.state[this.width * y + x];
		}
	}
	boardCopy.tokensCount = this.tokensCount;
	return boardCopy;
};

Board.prototype.isFull = function() {
	return this.width * this.height == this.tokensCount;
};

Board.prototype.hashCode = function() {
	return this.hash;
};

Board.prototype.calculateHashCode = function() {
	var code = "";
	for(var y = 0; y < this.height; y++) {
		for(var x = 0;  x < this.width; x++) {
			code += this.getTokenAt(x, y);
		}
	}
	this.hash = code;
};

Board.prototype.toString = function(newLineSeparator) {
	if(!newLineSeparator) {
		newLineSeparator = "\n";
	}
	
	var str = "";		
	for(var y = 0; y < this.height; y++) {
		for(var x = 0; x < this.width; x++) {
			var tokenVal = this.getTokenAt(x, y);
			if(tokenVal == TokenType.X) {
				str += "X";
			} else if(tokenVal == TokenType.O) {
				str += "O";
			} else {
				str += "_";
			}
		}
		str += newLineSeparator;
	}
	
	return str;
};