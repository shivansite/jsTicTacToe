var TokenType = {
	X:1,
	O:2,
	NONE:0
};

var DisplayLabelTokenType = {
		1: "X",
		2: "O",
		0: "Empty"
};

var DisplayLabelPlayerOwner = {
		10: "A.I.",
		100: "Human"
};

var DisplayLabelPlayerStatus = {
		1: "Player #1 wins",
		2: "Player #2 wins",
		0: "Draw",
		100: "No winner yet"		
};

var CELL_SIZE = 150;

var CELLS_X = 3;
var CELLS_Y = 3;
var CELLS_IN_A_ROW = 3;