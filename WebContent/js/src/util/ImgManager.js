/**
 * @Constructor
 */
function ImgManager() {
	
	this.loadImage = function(imgLocation) {
		var image = new Image();
		image.src=imgLocation;
		return image;
	};
};
