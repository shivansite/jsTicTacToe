function OnScreenDebugConsole() {
	this.debugConsoleDiv = $("#debugConsoleDiv");
	
	this.toConsole = function(message) {
		$("<span>" + message + "<br></span>").appendTo(this.debugConsoleDiv);
	};
	
	this.clear = function() {
		this.debugConsoleDiv.empty();
	};
}