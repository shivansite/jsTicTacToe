require.config({
    baseUrl: "js",
    paths: {
        jquery: "lib/jquery-1.11.1_"       
    }
});

require([
        "jquery",
        
        "src/finiteStateMachine/FiniteStateMachine",
        "src/finiteStateMachine/FSMState",
        
        "src/gameLoop/GameLoopConstants",
        "src/gameLoop/GameEntity",
        "src/gameLoop/GameLoop",
        
        "src/gameLogic/Board",
        "src/gameLogic/GameArbiter",
        "src/gameLogic/GameLogicConstants",
        "src/gameLogic/GameStateManager",
        
        "src/gameLogic/finiteStateMachine/FSMStateCheckPlayerStatus",
        "src/gameLogic/finiteStateMachine/FSMStateChoseNextPlayer",
        "src/gameLogic/finiteStateMachine/FSMStateGameEnded",
        "src/gameLogic/finiteStateMachine/FSMStateGetHumanInput",
        "src/gameLogic/finiteStateMachine/FSMStateMakeCPUMove",
        "src/gameLogic/finiteStateMachine/TicTacToeFSM",
        
        "src/miniMax/GameTree",
        "src/miniMax/IGameStateManager",
        "src/miniMax/MiniMaxConstants",
        "src/miniMax/Player",

        "src/inputManager/InputManager",
        "src/inputManager/InputManagerConstants",
        
        "src/gameEntities/Cell",
        "src/gameEntities/GameArea",
        
        "src/util/ImgManager",
        "src/util/OnScreenDebugConsole",
        
        "src/main/Main"],
         function(util) {
	    	main();
});

