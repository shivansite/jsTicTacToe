var DEBUG_CONSOLE;

function main() {
	DEBUG_CONSOLE = new OnScreenDebugConsole();	
	
	var imgManager = new ImgManager();	
	var imgGameArea = imgManager.loadImage("img/board.png");
	if(CELLS_X == 4) {
		var imgGameArea = imgManager.loadImage("img/board4x3.png");
	}
	var imgCellHighlight = imgManager.loadImage("img/cell_highlight.png");
	var imgTokenX = imgManager.loadImage("img/X.png");
	var imgTokenO = imgManager.loadImage("img/O.png");
	var imgInactive = imgManager.loadImage("img/inactive.png");
	var imgLoading = imgManager.loadImage("img/loading.png");
	
	var gameEntitiesLayer0 = new Array();
	var gameEntitiesLayer1 = new Array();	
	
	var idx = 0;
	for(var x = 0; x < CELLS_X; x++) {
		for(var y = 0; y < CELLS_Y; y++) {
			gameEntitiesLayer1[idx++] = new Cell(x, y, imgCellHighlight, imgTokenX, imgTokenO);
		}
	}
	gameEntitiesLayer0[0] = new GameArea(imgGameArea, imgInactive, imgLoading, gameEntitiesLayer1);
		
	var inputEventQueue = new Queue(100);
	new CanvasMouseListener(inputEventQueue);
	new ControlsInputManager(inputEventQueue);
	var finiteStateMachine = new TicTacToeFSM(new GameArbiter(), gameEntitiesLayer0[0], gameEntitiesLayer1);
	var gameLoop = new GameLoop(finiteStateMachine, gameEntitiesLayer0, gameEntitiesLayer1, inputEventQueue);
	gameLoop.start(CELLS_X * CELL_SIZE, CELLS_Y * CELL_SIZE);
};